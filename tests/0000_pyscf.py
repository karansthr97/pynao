
import numpy as np
from pyscf import gto

mol = gto.M( verbose = 1,
    atom = '''
        O     0    0        0
        H     0    -0.757   0.587
        H     0    0.757    0.587''', basis = 'cc-pvdz',)

print(mol)
