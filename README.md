# PyNAO

PyNAO the Python Numerical Atomic Orbitals.

PyNAO is designed to perform efficient many-body perturbation theory methods 
with numerical atomic orbitals. Actual implemented methods are

* TDDFT (light and electronic perturbation, i.e., EELS)
* G0W0 (GW is ready ??)
* BSE

The code needs inputs from previous DFT calculations. Supported DFT code are

* Siesta (https://departments.icmab.es/leem/siesta/)
* PySCF (https://sunqm.github.io/pyscf/)

## Installation

### Dependencies

In order to install PyNAO, the following packages must be installed

* git
* gcc
* gfortran
* build-essential
* liblapack-dev
* libfftw3-dev
* make
* cmake
* zlib1g-dev
* python3
* python3-pip

### Architecture file cmake.arch.inc

This file contains platform specific instruction for the compilation of the
Fortan and C libraries, such as BLAS and LAPACK libraries
definition (with MKL for example).

You can find examples in the folder `lib/cmake_user_inc_examples`

### Instruction examples

On ubuntu

    apt-get update
    apt-get install git gcc gfortran build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev python3 python3-pip
    pip3 install -r requirements.txt
    export CC=gcc && export FC=gfortran && export CXX=g++
    cp lib/cmake_user_inc_examples/cmake.user.inc-gnu lib/cmake.arch.inc
    python setup.py bdist_wheel
    pip install dist/pynao-0.1-py3-none-any.whl

### Installation via Anaconda

For optimal performances, we advise you to use Anaconda Python environment.
This for three reasons

* First, in Anaconda, NumPy and SciPy libraries are compiled with MKL Blas library
(offering in general better performances).
* You can easily use the MKL library included in Anaconda to compile PyNAO (even
if you could still installed MKL by yourself without Anaconda).
* At the moment, installing PySCF via pip gives a conflict with the `libgomp` library,
forbidding PyNAO to be compiled with openmp support. Installing PySCF via conda
solves this issues (see https://github.com/pyscf/pyscf/issues/603)

In the following, we assume Anaconda is installed under `${HOME}/anaconda3`.
To properly compile PyNAO with Anaconda, perform the following steps

    apt-get update
    apt-get install git gcc gfortran build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev python3 python3-pip
    mkdir -p ${HOME}/anaconda3/lib/mkl/lib
    ln -s ${HOME}/anaconda3/lib/libmkl* ${HOME}/anaconda3/lib/mkl/lib/
    export CC=gcc && export FC=gfortran && export CXX=g++
    pip install ase
    conda install -c pyscf pyscf

Now, go to `pynao` root folder

    cp lib/cmake_user_inc_examples/cmake.user.inc-singularity.anaconda.gnu.mkl lib/cmake.arch.inc

Edit the file `lib/cmake.arch.inc` to indicate the proper path to MKL library.
You should replace the line `set(MKLROOT "/opt/conda/lib/mkl/lib")` by
`set(MKLROOT "${HOME}/anaconda3/lib/mkl/lib")`

then you can finish the installation

    python setup.py bdist_wheel
    pip install dist/pynao-0.1-py3-none-any.whl

### GPU support

Some part of the code can be accelerated using Nvidia GPUs via the cuBlas library.
For this you need to install the `cupy` library

    pip install cupy

or via `conda`

    conda install cupy

You can then pass the argument `GPU=True` in order to use GPU for your calculations.
GPU will mainly impact performances for large systems.

## Know issues

See list of issues in the Gitlab repository, https://gitlab.com/mbarbry/pynao/-/issues

## History

PyNAO was originally developed in the PySCF package under the name PySCF-NAO
(https://github.com/cfm-mpc/pyscf/tree/nao2), and before this, it was a Fortran
code called `MBPT_LCAO` (http://mbpt-domiprod.wikidot.com/).

## Citing PySCF/NAO

Please, cite the following paper when using PyNAO

* PySCF-NAO: An efficient and flexible implementation of linear response
time-dependent density functional theory with numerical atomic orbitals,
P. Koval, M. Barbry and D. Sanchez-Portal, Computer Physics Communications,
2019, 10.1016/j.cpc.2018.08.004 (https://www.sciencedirect.com/science/article/pii/S0010465518302996)
* A Parallel Iterative Method for Computing Molecular Absorption Spectra,
P. Koval, D. Foerster, and O. Coulaud, J. Chem. Theo. Comput. 2010, 10.1021/ct100280x
(https://pubs.acs.org/doi/abs/10.1021/ct100280x)

PySCF papers:

* PySCF: the Python-based Simulations of Chemistry Framework, Q. Sun, T. C. Berkelbach,
N. S. Blunt, G. H. Booth, S. Guo, Z. Li, J. Liu, J. McClain, E. R. Sayfutyarova,
S. Sharma, S. Wouters, G. K.-L. Chan (2018), PySCF: the Python‐based simulations
of chemistry framework. WIREs Comput. Mol. Sci., 8: e1340. doi:10.1002/wcms.1340

The methods for TDDFT has been extensively described in M. Barbry Ph.D thesis

* Plasmons in Nanoparticles: Atomistic Ab Initio Theory for Large Systems, M. Barbry,
(https://cfm.ehu.es/cfm_news/phd-thesis-defense-marc-barbry/)

## Bug report

* Barbry Marc <marc.barbry@mailoo.org>
* Koval Peter <koval.peter@gmail.com>
* Masoud Mansouri <ma.mansoury@gmail.com>
